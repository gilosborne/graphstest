//
//  FirstViewController.swift
//  Charting Demo
//
//  Created by Nikhil Kalra on 12/5/14.
//  Copyright (c) 2014 Nikhil Kalra. All rights reserved.
//

import UIKit
import JBChart

class FirstViewController: UIViewController, JBBarChartViewDelegate, JBBarChartViewDataSource {

    @IBOutlet weak var barChart: JBBarChartView!
    @IBOutlet var weeklyAvgBar: UIView!
    @IBOutlet weak var avgLabel: UILabel!

    
    //var testArray = ["Jun 29, 2016", "Jul 1, 2016", "Jul 2, 2016", "Jul 3, 2016", "Jul 6, 2016", "Jul 7, 2016", "Jul 10, 2016", "Jul 12, 2016", "Jul 13, 2016", "Jul 14, 2016", "Jul 18, 2016", "Jul 19, 2016", "Jul 20, 2016", "Jul 28, 2016"]
    var testArray = [String]()
    var goal = "4"
    var avg = "6.9"
    
    
    let todaysDate = NSDate()
    
    var chartData = [Int]()
    var lastMonth = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        updateLabel()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        testArray = ["Jul 20, 2016", "Jul 28, 2016"]
        //create array of last 30 days
        for day in 0 ..< 30 {
            
            let previousDate = NSCalendar.currentCalendar()
                .dateByAddingUnit(
                    .Day,
                    value: -day,
                    toDate: todaysDate,
                    options: []
            )
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "MMM dd, yyyy"
            
            let addedDate = NSDateFormatter.localizedStringFromDate(previousDate!, dateStyle: NSDateFormatterStyle.MediumStyle, timeStyle: NSDateFormatterStyle.NoStyle)
            
            lastMonth.append(addedDate)
            
            
            
        }
        
        
        for day in 0 ..< 30 {
            if (testArray.contains(lastMonth[29-day])){
                chartData.append(10)
            }else{
                chartData.append(0)
            }
            
        }
        
        print(self)
        
        barChart.delegate = self
        barChart.dataSource = self
        barChart.minimumValue = 0
        barChart.maximumValue = 10
        
        barChart.reloadData()
        
        barChart.setState(.Collapsed, animated: false)
    }
    
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        // our code
        barChart.reloadData()
        
        _ = NSTimer.scheduledTimerWithTimeInterval(0.6, target: self, selector: #selector(FirstViewController.showChart), userInfo: nil, repeats: false)
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        hideChart()
    }
    
    func hideChart() {
        barChart.setState(.Collapsed, animated: true)
    }
    
    func showChart() {
        barChart.setState(.Expanded, animated: true)
    }
    
    // MARK: JBBarChartView
    
    func numberOfBarsInBarChartView(barChartView: JBBarChartView!) -> UInt {
        return UInt(chartData.count)
    }
    
    func barChartView(barChartView: JBBarChartView!, heightForBarViewAtIndex index: UInt) -> CGFloat {
        return CGFloat(chartData[Int(index)])
    }
    
    func barChartView(barChartView: JBBarChartView!, colorForBarViewAtIndex index: UInt) -> UIColor! {
        return (index % 2 == 0) ? UIColor.redColor() : UIColor.redColor()
    }
    
    func updateLabel(){
        
        avgLabel.text = avg
        
        let avgPerc = CGFloat(Double(avg)!)/7
        
        weeklyAvgBar.translatesAutoresizingMaskIntoConstraints = true
        
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        print(Double(avg))
        var averageGraph = screenSize.width*avgPerc
        
        UIView.animateWithDuration(2, animations: {
            self.weeklyAvgBar.frame =
                CGRect(x: 0, y: 300, width: averageGraph, height: 50)
        })
        
    }
    

}

